﻿using System.Collections;
using UnityEngine;
using Valve.VR;

public class Gun : MonoBehaviour
{
    private float dmg;
    private float impactForce;
    private float range;
    private float bulletSize;
    private float bulletSpeed;
    private float fireRate;

    void Update()
    {
        
    }

    public void doubleTap()
    {
        fireRate = fireRate * 2;
    }

    public float getDmg()
    {
        return dmg;
    }

    public float getImpactForce()
    {
        return impactForce;
    }

    public float getFireRate()
    {
        return fireRate;
    }

    public float getRange()
    {
        return range;
    }

    public float getBulletSize()
    {
        return bulletSize;
    }

    public float getBulletSpeed()
    {
        return bulletSpeed;
    }
}
