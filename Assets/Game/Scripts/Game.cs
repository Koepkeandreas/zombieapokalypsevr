﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public GameObject Zombie;
    public AudioClip newRoundSound;
    public AudioClip endRoundSound;
    public AudioClip easterEggSong;
    public Transform StartPoint;
    public Transform ManuePoint;

    private float maxZombieSpawn = 15f;
    private float startZombies = 5f;
    private float zombiesPerWave = 2f;
    private float wave = 1f;
    private bool waveIsLoading = true;
    private float zombieanz = 0f;
    private float spawnedZombies = 0f;
    private Transform[] ZombieSpawners;
    private float wavePauseTime = 10f;
    private float spawnZombiePauseTime;

    private float price;
    private float maxMuni;
    private float magazin;
    private float maxMagazin;
    private bool canStart = true;
    private bool game = false;

    public void ExitGame()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void StartGame()
    {
        gameObject.transform.position = StartPoint.position;
        gameObject.transform.rotation= StartPoint.rotation;
        game = true;
        gameObject.transform.GetChild(0).Find("Controller (right)").Find("Pointer").gameObject.SetActive(false);
        GetComponent<Player>().StartGame();
        spawnZombiePauseTime = Random.Range(1f, 4f);
        if (ZombieSpawners != null && Zombie && ZombieSpawners.Length > 0) startGame();
        else canStart = false;
    }

    public void GameOver()
    {
        game = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public bool getGameStatus ()
    {
        return game;
    }

    void Update()
    {
        if (game)
        {
            if(!canStart) {
                if(ZombieSpawners != null) {
                    canStart = true;
                    startGame();
                }
                else canStart = false;
            }
            zombieanz = GameObject.FindGameObjectsWithTag("Zombie").Length;

            if (ZombieSpawners != null && Zombie && ZombieSpawners.Length > 0 && GameObject.Find("Zombie(Clone)") == null && !waveIsLoading)
            {
                GetComponent<AudioSource>().PlayOneShot(endRoundSound);
                waveIsLoading = true;
                spawnedZombies = 0f;
                StartCoroutine(newWave());
            }
        }
    }

    public void playEasterEggSong ()
    {
        GetComponent<AudioSource>().PlayOneShot(easterEggSong);
    }

    public void startGame()
    {
        showWave();
        StartCoroutine(spawnZombies());
    }

    public IEnumerator newWave()
    {
        WaitForSeconds wait = new WaitForSeconds(wavePauseTime);
        yield return wait;
        GetComponent<AudioSource>().PlayOneShot(newRoundSound);
        wave++;
        showWave();
        StartCoroutine(spawnZombies());
    }

    public IEnumerator spawnZombies()
    {
        for(;zombieanz < maxZombieSpawn && spawnedZombies < startZombies + (wave-1) * zombiesPerWave;){
            int randomSpawner = Mathf.RoundToInt(Random.Range(0f, ZombieSpawners.Length - 1f));
            GameObject ZombieClone = Instantiate(Zombie, ZombieSpawners[randomSpawner].transform.position, ZombieSpawners[randomSpawner].transform.rotation) as GameObject;
            ZombieClone.SetActive(true);
            spawnedZombies++;
            if (spawnedZombies == startZombies + (wave-1) * zombiesPerWave) waveIsLoading = false;
            yield return new WaitForSeconds(spawnZombiePauseTime);
        }
        if(waveIsLoading && spawnedZombies < startZombies + (wave-1) * zombiesPerWave){
            yield return new WaitForSeconds(3f);
            StartCoroutine(spawnZombies());
        }
    }

    public void showWave()
    {
        if(transform.GetChild(0).Find("Camera (eye)")){
            if(wave == 11f) {
                transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Wave").gameObject.SetActive(false);
                transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("WaveNumber").gameObject.SetActive(true);
            }
            if(wave <= 10f){
                transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Wave").GetChild(Mathf.RoundToInt(wave-1f)).gameObject.SetActive(true);
                transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Wave").GetChild(Mathf.RoundToInt(wave-1f)).GetComponent<Animation>().Play("newWave");
            }
            else {
                transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("WaveNumber").GetComponent<Animation>().Play("newWave");
                transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("WaveNumber").GetComponent<UnityEngine.UI.Text>().text = wave.ToString();
            }
        }
    }

    public float getWeaponStats(string weapon, string value)
    {
        switch (weapon)
        {
            case "Bennelli_M4":
                {
                    price = 1500;
                    magazin = 6f;
                    maxMagazin = 6f;
                    maxMuni = 54f;
                    break;
                }
            case "AK74":
                {
                    price = 1750;
                    magazin = 25f;
                    maxMagazin = 25f;
                    maxMuni = 170f;
                    break;
                }
            case "M4_8":
                {
                    price = 1500;
                    magazin = 30f;
                    maxMagazin = 30f;
                    maxMuni = 210f;
                    break;
                }
            case "M107":
                {
                    price = 1500;
                    magazin = 5f;
                    maxMagazin = 5f;
                    maxMuni = 20f;
                    break;
                }
            case "M249":
                {
                    price = 3500;
                    magazin = 60f;
                    maxMagazin = 60f;
                    maxMuni = 350f;
                    break;
                }
            case "M1911":
                {
                    price = 250;
                    magazin = 8f;
                    maxMagazin = 8f;
                    maxMuni = 32f;
                    break;
                }
            case "RPG7":
                {
                    price = 3500;
                    magazin = 1f;
                    maxMagazin = 1f;
                    maxMuni = 20f;
                    break;
                }
            case "Uzi":
                {
                    price = 1000;
                    magazin = 24f;
                    maxMagazin = 24f;
                    maxMuni = 120f;
                    break;
                }
        }

        switch (value)
        {
            case "price":
                return price;
            case "magazin":
                return magazin;
            case "maxMagazin":
                return maxMagazin;
            case "maxMuni":
                return maxMuni;
        }

        return 0;
    }

    public void setSpawner (Transform[] spawner) {
        ZombieSpawners = spawner;
    }
}
