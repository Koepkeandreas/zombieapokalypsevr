﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ReloadScript : MonoBehaviour
{
    public SteamVR_Action_Boolean m_ReloadPress = null;

    private Interactable m_CurrentInteractable = null;
    private GameObject magazin;

    void Update()
    {
        m_CurrentInteractable = GetComponent<Hand>().getGrabbeldObject();

        if (m_CurrentInteractable && m_CurrentInteractable.transform.Find("Magazin"))
        {
            magazin = m_CurrentInteractable.transform.Find("Magazin").gameObject;
        }

        // If A pressed, reload
        if (magazin && m_CurrentInteractable && m_ReloadPress.GetStateDown(SteamVR_Input_Sources.Any) && transform.parent.parent.gameObject.GetComponent<Player>().reload(false))
        {
            reload(magazin);
        }
    }

    public void reload(GameObject magazin)
    {
        if(m_CurrentInteractable.transform.name != "RPG7" && m_CurrentInteractable.transform.name != "RPG7Upgrade")
        {
            magazin.GetComponent<Rigidbody>().isKinematic = false;
            gameObject.transform.parent.Find("Controller (left)").GetComponent<SpawnMagazinScript>().spawnMagazin(magazin);
            magazin.GetComponent<Rigidbody>().useGravity = true;
            Destroy(magazin, 0.5f);
        }
        else
        {
            gameObject.transform.parent.Find("Controller (left)").GetComponent<SpawnMagazinScript>().spawnMagazin(magazin);
        }
    }
}
