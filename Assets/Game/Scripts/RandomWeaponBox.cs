﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomWeaponBox : MonoBehaviour
{
    public GameObject[] weapons;
    public Transform WeaponSpawnPoint;

    private GameObject openWeapon = null;

    void Update()
    {
        if (openWeapon != null)
        {
            openWeapon.transform.position = WeaponSpawnPoint.transform.position;
        }

        if (gameObject.GetComponent<Animation>().IsPlaying("OpenWeaponBox"))
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<BoxCollider>().enabled = true;
            if (openWeapon)
            {
                Destroy(openWeapon);
            }
        }
    }

    public void openBox()
    {
        gameObject.GetComponent<Animation>().Play("OpenWeaponBox");
        GetComponent<AudioSource>().Play();
        StartCoroutine(randomWeapon(0.2f));
    }

    private IEnumerator randomWeapon(float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);

        for (float i = 1f; i <= 15f; i++)
        {
            if (openWeapon != null) Destroy(openWeapon);
            int randomWeapon = Mathf.RoundToInt(Random.Range(0f, weapons.Length - 1f));
            openWeapon = Instantiate(weapons[randomWeapon], WeaponSpawnPoint.transform.position, WeaponSpawnPoint.transform.rotation) as GameObject;
            openWeapon.GetComponent<BoxCollider>().enabled = false;
            openWeapon.GetComponent<Rigidbody>().useGravity = false;
            openWeapon.GetComponent<Rigidbody>().isKinematic = true;
            yield return wait;
            if (i == 15f)
            {
                openWeapon.transform.SetParent(gameObject.transform);
                openWeapon.transform.name = openWeapon.transform.name.Replace("(Clone)", "");
                openWeapon.transform.name = openWeapon.transform.name + "(RandomWeaponBox)";
                openWeapon.GetComponent<BoxCollider>().enabled = true;
                Destroy(openWeapon, 6.5f);
            }
        }
    }

    public GameObject takeWeapon(){
        openWeapon.GetComponent<Rigidbody>().useGravity = true;
        openWeapon.GetComponent<Rigidbody>().isKinematic = false;
        openWeapon.GetComponent<BoxCollider>().enabled = false;
        GameObject weapon =  Instantiate(openWeapon, openWeapon.transform.position, openWeapon.transform.rotation) as GameObject;
        weapon.GetComponent<BoxCollider>().enabled = false;
        weapon.transform.name = openWeapon.transform.name.Replace("(Clone)","");
        weapon.transform.name = openWeapon.transform.name.Replace("(RandomWeaponBox)","");
        return weapon;
    }
}
