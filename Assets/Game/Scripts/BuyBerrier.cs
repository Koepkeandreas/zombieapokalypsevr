﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyBerrier : MonoBehaviour
{
    public GameObject[] parts;
    public Transform explosionPoint;

    public void open()
    {
        foreach(GameObject part in parts)
        {
            Rigidbody rb = part.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
                rb.useGravity = true;
                rb.AddExplosionForce(200f, explosionPoint.position, 4f, 2f);
            }
            Destroy(part, 2f);
        }
        Destroy(gameObject);
    }
}
