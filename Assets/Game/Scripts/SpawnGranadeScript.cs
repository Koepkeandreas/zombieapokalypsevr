﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class SpawnGranadeScript : MonoBehaviour
{
    public SteamVR_Action_Boolean m_SpawnGranade = null;
    public GameObject granadeObject;
    public Transform GranadeSpawnpoint;

    void Update(){
        float granadeAmount = transform.parent.parent.GetComponent<Player>().GetGranades();

        // If Y pressed, spawn Granade
        if (granadeObject && GranadeSpawnpoint && m_SpawnGranade.GetStateDown(SteamVR_Input_Sources.Any) && granadeAmount > 0f)
        {
            spawnGranade();
            transform.parent.parent.GetComponent<Player>().DecGranades();
        }
    }

    public void spawnGranade() {
        Instantiate(granadeObject, GranadeSpawnpoint.transform.position, GranadeSpawnpoint.transform.rotation);
    }
}
