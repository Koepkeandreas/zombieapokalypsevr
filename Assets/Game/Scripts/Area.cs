﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{
    public Transform[] spawner;
    public Transform AreaPoint;
    public bool areaOpen = false;

    public Transform[] getSpawner () {
        return spawner;
    }

    public Transform getAreaPoint () {
        return AreaPoint;
    }

    public bool isAreaOpen (){
        return areaOpen;
    }

    public void openArea () {
        areaOpen = true;
        foreach (Transform child in transform.Find("Doors"))
        {
            child.Find("BuyBerrier").GetComponent<BuyBerrier>().open();
        }
    }
}
